import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.ticker import NullFormatter
import seaborn as sns
import argparse
import joblib
    # MyFunctions
from utils.utils import load_data, get_corrcoef, hep_reweight_array, get_selection
    # Fixes relative imports
import sys
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # Current working directory. Used for relative imports of data files
cwd = path.abspath(path.dirname(__file__))+"/"

plt.style.use('seaborn-paper')
font_size = 15
plt.rcParams.update({'font.size'          : font_size,
                     'axes.titlesize'     : int(font_size*0.9),
                     'axes.labelsize'     : font_size,
                     'xtick.labelsize'    : int(font_size*0.8),
                     'ytick.labelsize'    : int(font_size*0.8),
                     'figure.figsize'     : (5,4),
                     'lines.markersize'   : 10,
                     'lines.linewidth'    : 3,
                     'legend.fontsize'    : 30,
                     'legend.handlelength': 3,
                     'text.usetex'        : False})

parser = argparse.ArgumentParser(description='IsoDecorr and select cleaned labels.')
parser.add_argument('--filename', action='store', type=str, required=True, help='Which h5 file to run on?')
parser.add_argument('--pwd-storage', action='store', type=str, required=False, help='Pwd of storage dir (default = cwd/storage/)', default=cwd + 'storage/')

args = parser.parse_args()

    # De-correlation of (transformed) ML isolation from ML PID:
def Decorrelate_TMLiso(TMLiso, TMLpid, vars):
        # Approximate variances and correlations obtained from data using (logit) transformed MLiso and MLpid variables:
    Var_MLiso_sig = vars['Var_MLiso_sig']
    Var_MLiso_bkg = vars['Var_MLiso_bkg']
    Var_MLiso_ave = 0.5*(Var_MLiso_sig + Var_MLiso_bkg)
    Var_MLpid_sig = vars['Var_MLpid_sig']
    Var_MLpid_bkg = vars['Var_MLpid_bkg']
    Var_MLpid_ave = 0.5*(Var_MLpid_sig + Var_MLpid_bkg)
    Corr_MLisoMLpid_sig = vars['Corr_MLisoMLpid_sig']
    Corr_MLisoMLpid_bkg = vars['Corr_MLisoMLpid_bkg']
    Corr_MLisoMLpid_ave = 0.5*(Corr_MLisoMLpid_sig + Corr_MLisoMLpid_bkg)
    
    TMLisoDeCorr = TMLiso - ( TMLpid * ( np.sqrt(Var_MLiso_ave)/np.sqrt(Var_MLpid_ave) ) * Corr_MLisoMLpid_ave )
    return TMLisoDeCorr
    # Logit transformation (to make "peaky" ML - here LightGBM - output more Gaussian):
def TransformMLoutput(MLvar):
    tau = 10.0
    TMLvar = -np.log( 1.0/ MLvar -1.0 ) / tau
    return TMLvar

show_plot = False
Save_sel  = False # This will reweight and create new dataset (takes a while)
    # The Following values need to be hard-coded! The plot will be immensely helpful to decide them!
bkg_box_pid = 0.0
bkg_box_iso = 0.0
sig_box_pid = 0.3
sig_box_iso = 0.0
    # Final label cuts
mass_cut_low = 81.0
mass_cut_high = 101.0
TMLiso_DeCorr_cut = 0.2
    # Importing Data and preds from initial iso and pid models.
var_list, data = load_data(filepath = args.pwd_storage + args.filename, setnames=['data/'])
pid_score = joblib.load(cwd + 'output/14_1463528748/scores/pid_data_preds_auc_LH_pdf_vars_test_') #25_2474229913/scores/pid_data_preds_auc_electron_vars_final_')
iso_score = joblib.load(cwd + 'output/9_3898251691/scores/pid_data_preds_auc_iso_test_')
    # Transformation of isolation ML score (to make it more Gaussian)
TMLiso_score = TransformMLoutput(pid_score)
TMLpid_score = TransformMLoutput(iso_score)
    # Determining the corrolations in the signal and background distributions of iso versus pid_score
    # This is done by isolating the background and signal distributions in the 2d histogram.
bkg_box = (TMLpid_score < bkg_box_pid) & (TMLiso_score < bkg_box_iso) & (TMLiso_score > -2.0) & (TMLpid_score > -0.6)
sig_box = (TMLpid_score > sig_box_pid) & (TMLiso_score > sig_box_iso) & (TMLiso_score < 0.6) & (TMLpid_score < 1.0)

DeCorr_INFO = {}
DeCorr_INFO['Corr_MLisoMLpid_bkg'] = get_corrcoef(TMLpid_score[bkg_box], TMLiso_score[bkg_box])
DeCorr_INFO['Corr_MLisoMLpid_sig'] = get_corrcoef(TMLpid_score[sig_box], TMLiso_score[sig_box])
DeCorr_INFO['Var_MLiso_bkg'] = np.var(TMLiso_score[bkg_box])
DeCorr_INFO['Var_MLiso_sig'] = np.var(TMLiso_score[sig_box])
DeCorr_INFO['Var_MLpid_bkg'] = np.var(TMLpid_score[bkg_box])
DeCorr_INFO['Var_MLpid_sig'] = np.var(TMLpid_score[sig_box])

    # 2d histogram
fig = plt.figure(1, figsize=(20, 8), facecolor='white')
    # define some gridding.
#axCbar1  = plt.subplot2grid( (9,20), (1,0), rowspan=8)
axHist2d = plt.subplot2grid( (9,20), (1,0), colspan=8, rowspan=8 )
axHistx  = plt.subplot2grid( (9,20), (0,0), colspan=8 )
axHisty  = plt.subplot2grid( (9,20), (1,8), rowspan=8 )
axMass   = plt.subplot2grid( (9,20), (1,10), colspan=8, rowspan=8)
axDeCorr = plt.subplot2grid( (9,20), (0,10), colspan=8)
axCbar2  = plt.subplot2grid( (9,20), (1,18), rowspan=8)

h_Corr, xedges, yedges, img = axHist2d.hist2d(TMLiso_score, TMLpid_score, bins=[100,100], range=[[-0.6, 1.0], [-2.0, 0.6]], norm=LogNorm(), vmax=10e3)

axHist2d.set_xlabel('TMLiso_score')
axHist2d.set_ylabel('TMLpid_score')

#axHist2d.imshow(h_Corr.T, interpolation='nearest', aspect='auto', norm=LogNorm())
#sns.heatmap(h_Corr.T, cmap='viridis', ax=axHist2d, cbar=False)
#axHistx.hist(TMLiso_score, bins=xedges, histtype='step', alpha=1.0, edgecolor='grey')
axHistx.hist(TMLiso_score[data['p_LHLoose'] == 1], bins=xedges, facecolor='blue', alpha=0.6, edgecolor='None')
axHistx.hist(TMLiso_score[data['p_LHLoose'] == 0], bins=xedges, facecolor='red', alpha=0.6, edgecolor='None')

#axHisty.hist(TMLpid_score, bins=yedges, histtype='step', alpha=1.0, orientation='horizontal', edgecolor='grey')
axHisty.hist(TMLpid_score[data['p_LHLoose'] == 1], bins=yedges, facecolor='blue', alpha=0.6, orientation='horizontal', edgecolor='None')
axHisty.hist(TMLpid_score[data['p_LHLoose'] == 0], bins=yedges, facecolor='red', alpha=0.6, orientation='horizontal', edgecolor='None')

axHist2d.invert_yaxis()

axHistx.set_xlim( [xedges.min(), xedges.max()] )
axHisty.set_ylim( [yedges.min(), yedges.max()] )
axHist2d.set_ylim( [ axHist2d.get_ylim()[1], axHist2d.get_ylim()[0] ] )

nullfmt   = NullFormatter()
axHistx.xaxis.set_major_formatter(nullfmt)
axHistx.yaxis.set_major_formatter(nullfmt)
axHisty.xaxis.set_major_formatter(nullfmt)
axHisty.yaxis.set_major_formatter(nullfmt)

axHistx.spines['top'].set_visible(False)
axHistx.spines['right'].set_visible(False)
axHistx.spines['left'].set_visible(False)
axHisty.spines['top'].set_visible(False)
axHisty.spines['bottom'].set_visible(False)
axHisty.spines['right'].set_visible(False)

axHistx.set_xticks([])
axHistx.set_yticks([])
axHisty.set_xticks([])
axHisty.set_yticks([])

    # Bkg box
axHist2d.plot([-0.6,bkg_box_pid], [bkg_box_iso,bkg_box_iso], color='grey', linestyle='--')
axHist2d.plot([bkg_box_pid, bkg_box_pid], [-2.0, bkg_box_iso], color='grey', linestyle='--')
    # Sig box
axHist2d.plot([sig_box_pid, 1.0], [sig_box_iso,sig_box_iso], color='grey', linestyle='--')
axHist2d.plot([sig_box_pid, sig_box_pid], [sig_box_iso, 0.6], color='grey', linestyle='--')
    # Correlations
axHist2d.text(-0.43, 0.06, fr'$\rho_{{\mathrm{{bkg}}}} = {DeCorr_INFO["Corr_MLisoMLpid_bkg"]:.3f}$', color='black', bbox=dict(facecolor='white', alpha=0.25))
axHist2d.text(0.3, -0.1, fr'$\rho_{{\mathrm{{sig}}}} = {DeCorr_INFO["Corr_MLisoMLpid_sig"]:.3f}$', color='black', bbox=dict(facecolor='white', alpha=0.25))

    # De-correlation (from ML PID)
TMLiso_DeCorr = Decorrelate_TMLiso(TMLiso_score, TMLpid_score, DeCorr_INFO)
h_Corr, xedges, yedges, img = axMass.hist2d(TMLiso_DeCorr, data['ll_m'], bins=[100, 100], range=[[-0.5, 1.0], [50, 120]], norm=LogNorm(), vmax=10e3)
plt.colorbar(img, cax=axCbar2)
axMass.set_xlabel('TMLiso_DeCorr')
axMass.set_ylabel(fr'$m_{{ll}}$')

#axDeCorr.hist(TMLiso_DeCorr, bins=xedges, histtype='step', alpha=1.0, edgecolor='grey')
axDeCorr.hist(TMLiso_DeCorr[data['p_LHLoose'] == 1], bins=xedges, facecolor='blue', alpha=0.6, edgecolor='None')
axDeCorr.hist(TMLiso_DeCorr[data['p_LHLoose'] == 0], bins=xedges, facecolor='red', alpha=0.6, edgecolor='None')

axDeCorr.set_xlim( [xedges.min(), xedges.max()] )
axMass.set_ylim( [ axMass.get_ylim()[1], axMass.get_ylim()[0] ] )

axDeCorr.xaxis.set_major_formatter(nullfmt)
axDeCorr.yaxis.set_major_formatter(nullfmt)

axDeCorr.spines['top'].set_visible(False)
axDeCorr.spines['right'].set_visible(False)
axDeCorr.spines['left'].set_visible(False)

axDeCorr.set_xticks([])
axDeCorr.set_yticks([])

    # Sig box
axMass.plot([-0.5, 1.0], [mass_cut_low, mass_cut_low], color='grey', linestyle='--')
axMass.plot([-0.5, 1.0], [mass_cut_high, mass_cut_high], color='grey', linestyle='--')
axMass.plot([TMLiso_DeCorr_cut, TMLiso_DeCorr_cut], [50, 120], color='grey', linestyle='--')
    # Sig and bkg text
axMass.text(0.23, 99.5, fr'Signal', bbox=dict(facecolor='white', alpha=0.25))
axMass.text(-0.47, 79.5, fr'Background', bbox=dict(facecolor='white', alpha=0.25))
axMass.text(-0.47, 104, fr'Background', bbox=dict(facecolor='white', alpha=0.25))
fig.savefig(cwd + 'MLtransform_masterplot.pdf')
if(show_plot): plt.show()

bkg_cut = (TMLiso_DeCorr < TMLiso_DeCorr_cut) & ((data['ll_m'] < mass_cut_low) | (data['ll_m'] > mass_cut_high))
sig_cut = (TMLiso_DeCorr > TMLiso_DeCorr_cut) & ((data['ll_m'] > mass_cut_low) & (data['ll_m'] < mass_cut_high))
    # Selection of cleaned labels (Final labels becomes TruthSelection)
sig_sel = ( (data['TruthSelection'] == 1) & sig_cut )
bkg_sel1 = ( (data['TruthSelection'] == 0) & (TMLiso_DeCorr < TMLiso_DeCorr_cut) & (data['tag_type'] != 1) )
bkg_sel2 = ( (data['TruthSelection'] == 0) & (data['tag_type'] == 1) & bkg_cut )
    # If you pass, we select the event for further training.
event_sel = (sig_sel | bkg_sel1 | bkg_sel2) & (data['p_et_calo'] > 14.5)

if(Save_sel):
        # Select events
    data_final = data[event_sel]
    
        # Select 90/10 split for train/val
    selection = get_selection(len(data_final), 0.1)
    data_final_train = data_final[selection == 0]
    data_final_val   = data_final[selection == 1]
    
    data_final_train = hep_reweight_array(data_final_train, data_final_train['TruthSelection'])
    data_final_val = hep_reweight_array(data_final_val, data_final_val['TruthSelection'])
    
    with h5py.File(args.pwd_storage + 'data17_final_pid_sel.h5', 'w') as hf:
        for var in var_list + ['weight']:
            hf.create_dataset('train/' + var, data=data_final_train[var], compression = 'lzf')
            hf.create_dataset('val/' + var, data=data_final_val[var], compression = 'lzf')




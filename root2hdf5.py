#!/usr/bin/env python
# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings('ignore', 'ROOT .+ is currently active but you ')
warnings.filterwarnings('ignore', 'numpy .+ is currently installed but you ')

import re
import h5py
import json
import numpy as np
import logging as log
import multiprocessing
from subprocess import call
import ROOT
import argparse
import gc
import os

from utils.root2hdf5_utils import mkdir
from utils.variableLists import remove_branches

# Logging style and level
log.basicConfig(format='[%(levelname)s] %(message)s', level=log.INFO)

# Command line options
parser = argparse.ArgumentParser(description="Convert ROOT files with nested structure into flat HDF5 files.")
parser.add_argument('--tag', action='store', type=str, required=True,
                    help='Tag the data category (Zee, Wev, etc.).')
parser.add_argument('--stop', action='store', default=None, type=int,
                    help='Maximum number of events to read.')
parser.add_argument('--max-processes', action='store', default=10, type=int,
                    help='Maximum number of concurrent processes to use.')
parser.add_argument('--outdir', action='store', default="output/full", type=str,
                    help='Output directory.')
parser.add_argument('--max-input-files', action='store', default=0, type=int,
                    help='Use at most X files. Default is all.')
parser.add_argument('paths', type=str, nargs='+',
                    help='ROOT file(s) to be converted.')
parser.add_argument('--selection', action='store', default=None, type=str,
                    help='selection string on tree')
parser.add_argument('--datatype', action='store', default='MC', type=str,
                    choices=['MC', 'Data'], help='Real Data or MC')
parser.add_argument('--electron', action='store', default=True, type=int,
                    help='Whether to save the electron or the photon tree')

args = parser.parse_args()
# Validate arguments
if not args.paths:
    log.error("No ROOT files were specified.")
    quit()

if args.max_processes > 20:
    log.error("The requested number of processes ({}) is excessive (>20). Exiting.".format(args.max_processes))
    quit()

# Maximum number of events
if args.stop is not None:
    args.stop = int(args.stop)
else:
    args.stop = 100_000_000

# Standard selection is no two tags, and tag must be electron (this doesn't work for background or dumps which have no tags)
if args.selection == None:
    args.selection = ""

# Make and set the output directory to tag, if it doesn't already exist
# Will stop if the output already exists since re-running is either not needed or unwanted
# If it's wanted, delete the output first yourself
if args.electron:
    args.outdir+="/"+args.datatype+"/electron/"+args.tag+"/"
else:
    args.outdir+="/"+args.datatype+"/photon/"+args.tag+"/"    
if os.path.exists(args.outdir):
    log.error("Output already exists - please remove yourself.")
    quit()
else:
    mkdir(args.outdir)

# Sort paths, because it's nice
args.paths = sorted(args.paths)

# File number counter (incremented first in loop)
counter = -1


#============================================================================
# Functions
#============================================================================

def converter(arguments):
    """
    Process converting standard-format ROOT file to HDF5 file with cell
    content.

    Arguments:
        path: Path to the ROOT file to be converted.
        args: Namespace containing command-line arguments, to configure the
            reading and writing of files.

    Returns:
        Converted data in numpy array format
    """
    global args
    # Unpack arguments
    index, counter, path, start, stop = arguments

    # Suppress warnings like these when loading the files: TClass::Init:0: RuntimeWarning: no dictionary for class [bla] is available
    ROOT.gErrorIgnoreLevel = ROOT.kError

    index_edges = list(map(int, np.linspace(start, stop, 10, endpoint=True)))
    index_ranges = zip(index_edges[:-1], index_edges[1:])

    import root_numpy
    # Read-in data from ROOT.TTree
    if args.electron:
        all_branches = root_numpy.list_branches(path, 'el_tree')
    else:
        all_branches = root_numpy.list_branches(path, 'ga_tree')# ga_tree
    remove = remove_branches() + ['em_barrel_Lr0', 'em_barrel_Lr1','em_barrel_Lr2','em_barrel_Lr3',
                'em_endcap_Lr0', 'em_endcap_Lr1','em_endcap_Lr2','em_endcap_Lr3',
                'lar_endcap_Lr0', 'lar_endcap_Lr1','lar_endcap_Lr2','lar_endcap_Lr3',
                'tile_barrel_Lr1','tile_barrel_Lr2','tile_barrel_Lr3', 'tile_gap_Lr1',
                'time_em_barrel_Lr0', 'time_em_barrel_Lr1','time_em_barrel_Lr2','time_em_barrel_Lr3',
                'time_em_endcap_Lr0', 'time_em_endcap_Lr1','time_em_endcap_Lr2','time_em_endcap_Lr3',
                'time_lar_endcap_Lr0', 'time_lar_endcap_Lr1','time_lar_endcap_Lr2','time_lar_endcap_Lr3',
                'time_tile_barrel_Lr1','time_tile_barrel_Lr2','time_tile_barrel_Lr3', 'time_tile_gap_Lr1',
                'gain_em_barrel_Lr0', 'gain_em_barrel_Lr1','gain_em_barrel_Lr2','gain_em_barrel_Lr3',
                'gain_em_endcap_Lr0', 'gain_em_endcap_Lr1','gain_em_endcap_Lr2','gain_em_endcap_Lr3',
                'gain_lar_endcap_Lr0', 'gain_lar_endcap_Lr1','gain_lar_endcap_Lr2','gain_lar_endcap_Lr3',
                'gain_tile_barrel_Lr1','gain_tile_barrel_Lr2','gain_tile_barrel_Lr3', 'gain_tile_gap_Lr1',
                'noise_em_barrel_Lr0', 'noise_em_barrel_Lr1','noise_em_barrel_Lr2','noise_em_barrel_Lr3',
                'noise_em_endcap_Lr0', 'noise_em_endcap_Lr1','noise_em_endcap_Lr2','noise_em_endcap_Lr3',
                'noise_lar_endcap_Lr0', 'noise_lar_endcap_Lr1','noise_lar_endcap_Lr2','noise_lar_endcap_Lr3',
                'noise_tile_barrel_Lr1','noise_tile_barrel_Lr2','noise_tile_barrel_Lr3', 'noise_tile_gap_Lr1']
    keep_branches = sorted(list(set(all_branches)-set(remove)))
    one_evts_array = []
    for i, (loop_start, loop_stop) in enumerate(index_ranges): 
        if args.electron:
            array = root_numpy.root2array(path, 'el_tree', start=loop_start, stop=loop_stop, selection=args.selection, branches = keep_branches, warn_missing_tree=True)
        else:
            array = root_numpy.root2array(path, 'ga_tree', start=loop_start, stop=loop_stop, selection=args.selection, branches = keep_branches, warn_missing_tree=True) # ga_tree

        ROOT.gErrorIgnoreLevel = ROOT.kInfo

        n_evts = len(array)
        # If NO events survived, it's probably the selection
        if n_evts == 0:
            return
        # If only one event survives ( can happen with small files) the tracks can't be saved properly???, for now add all of these and save them later
        if n_evts == 1:
            one_evts_array.append(array)
            continue
        # Convert to HDF5-ready format.
        data = convert_to_hdf5(array)
        
        if args.electron and args.datatype == 'MC':
            scale = scale_eventWeight(data['mcChannelNumber'][0])
            data['event_totalWeight'] *= scale
        # 
        # Save output of every subprocess to a file
        filename = '{:s}_{:04d}.h5'.format(args.tag, index)
        if counter == 0 and i == 0:
            saveToFile(filename, data)
        else:
            appendToFile(filename, data)
        
        del data, array
        gc.collect()
                
    # Add all arrays with only one event and save them to the output file
    if len(one_evts_array) > 1:
        one_evts_array = np.concatenate(one_evts_array)
        one_evts_data = convert_to_hdf5(one_evts_array)
        filename = '{:s}_{:04d}.h5'.format(args.tag, index)
        appendToFile(filename, one_evts_data)


def scale_eventWeight(MCChannelNumber):
    root_file = ROOT.TFile(f"/groups/hep/ehrke/storage/NtuplefromGrid/Hists/v06Combined/hist.mc.combined.{MCChannelNumber}.root")
    hist = root_file.Get("SumOfWeights")
    
    scale = ( hist.GetBinContent(4) / hist.GetBinContent(1) ) / hist.GetBinContent(2)
    root_file.Close()
    del root_file
    return scale


def convert_types_to_hdf5(x):
    """
    Variable-length containers are converted to `h5py.special_dtype` with the appropriate `vlen` type.

    Arguments:
        x: Numpy-type variable to be type-checked.

    Returns:
        Same numpy type for scalars and h5py dtype for variable-length containers.
    """
    if 'ndarray' in str(type(x)):
        try:
            return h5py.special_dtype(vlen=convert_types_to_hdf5(x[0]))
        except:
            return h5py.special_dtype(vlen=np.float32)
    elif isinstance(x, str):
        return h5py.special_dtype(vlen=str)
    else:
        return x.dtype



def convert_to_hdf5(data):
    """
    Method to convert standard array to suitable format for classifier.

    Arguments:
        data: numpy array returned by root_numpy.tree2array, to be formatted.

    Returns:
        Flattened numpy recarray prepared for saving to HDF5 file.
    """

    # Format output as numpy structured arrays.
    formats = [convert_types_to_hdf5(data[0][var]) for var in data.dtype.names]
    for pair in zip(data.dtype.names, formats):
        try:
            np.dtype([pair])
        except:
            print("Problem for {}".format(pair))
    dtype  = np.dtype(list(zip(data.dtype.names, formats)))
    output = np.array([tuple([d[var] for var in data.dtype.names]) for d in data], dtype=dtype)

    return output


def imagesToArray(data):
    """
    Converts the images retrieved from the ROOT tree into the shape that is
    needed to feed them to keras
    """
    out = []
    for i in range(data.shape[0]):
        out.append(np.array([a.tolist() for a in data[i]]))

    return np.array(out)


def saveToFile(fname, data):
    """
    Simply saves data to fname.

    Arguments:
        fname: filename (directory is taken from script's args).
        data: numpy array returned by convert_to_hdf5(), or parts hereof.

    Returns:
        Nothing.
    """
    log.info("  Saving to {}".format(args.outdir + fname))
    with h5py.File(args.outdir + fname, 'w') as hf:
        for var in data.dtype.names:
            
            # Images need to be treated slightly different
            if var in ['em_barrel_Lr0', 'em_barrel_Lr1','em_barrel_Lr2','em_barrel_Lr3',
                        'em_endcap_Lr0', 'em_endcap_Lr1','em_endcap_Lr2','em_endcap_Lr3',
                        'lar_endcap_Lr0', 'lar_endcap_Lr1','lar_endcap_Lr2','lar_endcap_Lr3',
                        'tile_barrel_Lr1','tile_barrel_Lr2','tile_barrel_Lr3', 'tile_gap_Lr1',
                        'time_em_barrel_Lr0', 'time_em_barrel_Lr1','time_em_barrel_Lr2','time_em_barrel_Lr3',
                        'time_em_endcap_Lr0', 'time_em_endcap_Lr1','time_em_endcap_Lr2','time_em_endcap_Lr3',
                        'time_lar_endcap_Lr0', 'time_lar_endcap_Lr1','time_lar_endcap_Lr2','time_lar_endcap_Lr3',
                        'time_tile_barrel_Lr1','time_tile_barrel_Lr2','time_tile_barrel_Lr3', 'time_tile_gap_Lr1',
                        'gain_em_barrel_Lr0', 'gain_em_barrel_Lr1','gain_em_barrel_Lr2','gain_em_barrel_Lr3',
                        'gain_em_endcap_Lr0', 'gain_em_endcap_Lr1','gain_em_endcap_Lr2','gain_em_endcap_Lr3',
                        'gain_lar_endcap_Lr0', 'gain_lar_endcap_Lr1','gain_lar_endcap_Lr2','gain_lar_endcap_Lr3',
                        'gain_tile_barrel_Lr1','gain_tile_barrel_Lr2','gain_tile_barrel_Lr3', 'gain_tile_gap_Lr1',
                        'noise_em_barrel_Lr0', 'noise_em_barrel_Lr1','noise_em_barrel_Lr2','noise_em_barrel_Lr3',
                        'noise_em_endcap_Lr0', 'noise_em_endcap_Lr1','noise_em_endcap_Lr2','noise_em_endcap_Lr3',
                        'noise_lar_endcap_Lr0', 'noise_lar_endcap_Lr1','noise_lar_endcap_Lr2','noise_lar_endcap_Lr3',
                        'noise_tile_barrel_Lr1','noise_tile_barrel_Lr2','noise_tile_barrel_Lr3', 'noise_tile_gap_Lr1']:
                array = imagesToArray(data[f'{var}'])
                hf.create_dataset( f'{var}', data=array, chunks=True, maxshape= (None, None, None) , compression='lzf' )
            # Do not save the variable 'BC_bunchIntensities' or cell information
            elif var == 'BC_bunchIntensities' or "p_cell_" in var:
                continue
            # Create the dataset and save the array
            else:
                hf.create_dataset( f'{var}', data=data[f'{var}'], chunks=True, maxshape= (None,) , compression='lzf' )



def appendToFile(fname, data):
    """
    Simply appends data to fname.

    Arguments:
        fname: filename (directory is taken from script's args).
        data: numpy array returned by convert_to_hdf5(), or parts hereof.

    Returns:
        Nothing.
    """
    log.info("  Appending to {}".format(args.outdir + fname))
    with h5py.File(args.outdir + fname, 'a') as hf:
        for var in data.dtype.names:

            array = data[f'{var}']
            # Images need to be treated slightly different
            if var in ['em_barrel_Lr0', 'em_barrel_Lr1','em_barrel_Lr2','em_barrel_Lr3',
                        'em_endcap_Lr0', 'em_endcap_Lr1','em_endcap_Lr2','em_endcap_Lr3',
                        'lar_endcap_Lr0', 'lar_endcap_Lr1','lar_endcap_Lr2','lar_endcap_Lr3',
                        'tile_barrel_Lr1','tile_barrel_Lr2','tile_barrel_Lr3', 'tile_gap_Lr1',
                        'time_em_barrel_Lr0', 'time_em_barrel_Lr1','time_em_barrel_Lr2','time_em_barrel_Lr3',
                        'time_em_endcap_Lr0', 'time_em_endcap_Lr1','time_em_endcap_Lr2','time_em_endcap_Lr3',
                        'time_lar_endcap_Lr0', 'time_lar_endcap_Lr1','time_lar_endcap_Lr2','time_lar_endcap_Lr3',
                        'time_tile_barrel_Lr1','time_tile_barrel_Lr2','time_tile_barrel_Lr3', 'time_tile_gap_Lr1',
                        'gain_em_barrel_Lr0', 'gain_em_barrel_Lr1','gain_em_barrel_Lr2','gain_em_barrel_Lr3',
                        'gain_em_endcap_Lr0', 'gain_em_endcap_Lr1','gain_em_endcap_Lr2','gain_em_endcap_Lr3',
                        'gain_lar_endcap_Lr0', 'gain_lar_endcap_Lr1','gain_lar_endcap_Lr2','gain_lar_endcap_Lr3',
                        'gain_tile_barrel_Lr1','gain_tile_barrel_Lr2','gain_tile_barrel_Lr3', 'gain_tile_gap_Lr1',
                        'noise_em_barrel_Lr0', 'noise_em_barrel_Lr1','noise_em_barrel_Lr2','noise_em_barrel_Lr3',
                        'noise_em_endcap_Lr0', 'noise_em_endcap_Lr1','noise_em_endcap_Lr2','noise_em_endcap_Lr3',
                        'noise_lar_endcap_Lr0', 'noise_lar_endcap_Lr1','noise_lar_endcap_Lr2','noise_lar_endcap_Lr3',
                        'noise_tile_barrel_Lr1','noise_tile_barrel_Lr2','noise_tile_barrel_Lr3', 'noise_tile_gap_Lr1']:
                array = imagesToArray(data[f'{var}'])
                
            # Do not save the variable 'BC_bunchIntensities' or cell information
            if var == 'BC_bunchIntensities' or "p_cell_" in var:
                continue
            # Resize the existing dataset and save the array at the end
            else:
                hf[f'{var}'].resize((hf[f'{var}'].shape[0] + array.shape[0]), axis = 0)
                hf[f'{var}'][-array.shape[0]:] = array



#============================================================================
# Main
#============================================================================

# Make a pool of processes (this must come after the functions needed to run over since it apparently imports __main__ here)
pool = multiprocessing.Pool(processes=args.max_processes)

for path in args.paths:
    # Count which file we have made it to
    counter += 1

    # Stop if we've reached the maximum number of files
    if args.max_input_files > 0 and counter >= args.max_input_files:
        break

    # Suppress warnings like these when loading the files: TClass::Init:0: RuntimeWarning: no dictionary for class [bla] is available
    ROOT.gErrorIgnoreLevel = ROOT.kError

    # Read ROOT data from file
    print(path)
    f = ROOT.TFile(path, 'READ')
    if args.electron:
        tree = f.Get('el_tree')
    else:
        tree = f.Get('ga_tree') # ga_tree
    if tree.GetEntries() == 0: continue


    ROOT.gErrorIgnoreLevel = ROOT.kInfo

    # Set the number maximum number of entries to get to args.stop, but only if it's higher than the actual amount of events
    N = min(args.stop, tree.GetEntries())

    # Split indices into equally-sized batches
    index_edges = list(map(int, np.linspace(0, N, args.max_processes + 1, endpoint=True)))
    index_ranges = zip(index_edges[:-1], index_edges[1:])

    # Start conversion process(es) of ROOT data into numpy arrays and save them
    results = pool.map(converter, [(i, counter, path, start, stop) for i, (start, stop) in enumerate(index_ranges)])

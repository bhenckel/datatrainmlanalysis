from time import time
import numpy as np
import argparse
import h5py
import gc

    # My Functions
from utils.utils import hep_reweight_array, timediftostr, load_data, get_selection
    # Fixes relative imports
import sys
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # Current working directory. Used for relative imports of data files
cwd = path.abspath(path.dirname(__file__))+"/"

np.random.seed(42) # Setting Random Seed for consistent selection of data
"""
    # DeCorr_DataPreb.py ----------------------------------------------------------------------------------------------------------- #
    # This script takes a root ntuple (preferable highly skimmed) coverted to h5 with root2hdf5.py.                                  #
    # for electrons, the (locally) available data17 is collected (and skimmed) with collect_data17.cxx and converted to data17.h5    #
    # It selects 20 % of data to use for evaluation, saves that and the remaining 80 % in datasets called evl and data respectively. #
    # It then selects subset of data, labels, RWs and appends field, train/val split to both PID and ISO training data.              #
    # All 3 files are written to storage.                                                                                            #
    # ------------------------------------------------------------------------------------------------------------------------------ #
"""
parser = argparse.ArgumentParser(description='Data Preparation for IsoDeCorr Framework.')
parser.add_argument('--filename', action='store', type=str, required=True, help='Which h5 file to run on? (Ommit .h5)')
parser.add_argument('--pwd-storage', action='store', type=str, required=False, help='Pwd of storage dir (default = cwd/storage/)', default=cwd + 'storage/')

args = parser.parse_args()

    # Load Data
print(f'--- Importing Data ---')
t = time()

var_list, full_data = load_data(filepath = args.pwd_storage + args.filename + '.h5')
print(f'Finished Importing Data - Time spent: {timediftostr(t)}.')

    # Seperate 80 / 20 - and save 20 % in a test set.
print('--- Selecting Evaluation Set (20 %) ---')
evl_selection = get_selection(len(full_data), 0.2)
evl = full_data[evl_selection == 1]
data = full_data[evl_selection == 0]

del full_data
gc.collect()

print('--- Saving Data and Evl to storage ---')
t = time()

    # Saving data to storage
with h5py.File(args.pwd_storage + args.filename + "_prep_test.h5", 'w') as hf:
    for var in var_list:
        hf.create_dataset('data/' + var, data=data[var], compression = 'lzf')
        hf.create_dataset('evl/' + var, data=evl[var], compression = 'lzf')
print(f'Finished Saving to storage - Time spent: {timediftostr(t)}.')

    # PID Signal/bkg selection
    # Signal:     abs(ll_m - mZ) < 10 GeV & IsoCone/E < 0.06
    # Background: abs(ll_m - mZ) > 15 GeV & IsoCone/E > 0.15
    
print('--- Selecting PID Data and Labels ---')
PID_sig = (np.abs(data['ll_m'] - 91.2) < 10) & (data['p_ptvarcone30']/data['p_et_calo'] < 0.06)
PID_bkg = (np.abs(data['ll_m'] - 91.2) > 15) & (data['p_ptvarcone30']/data['p_et_calo'] > 0.15)
PID_event_sel = ((PID_sig) | (PID_bkg))
data_pid = data[PID_event_sel]
label_pid = ((np.abs(data_pid['ll_m'] - 91.2) < 10) & (data_pid['p_ptvarcone30']/data_pid['p_et_calo'] < 0.06))*1

print('--- Reweighting and appending labels ---')
t = time()

    # reweighting
data_pid = hep_reweight_array(data_pid, label_pid, is_MC=True)
    # Appending Label field
data_pid = np.lib.recfunctions.append_fields(data_pid, 'pid_labels', label_pid, usemask = False)
print(f'Finished Reweighting and Appending - Time spent: {timediftostr(t)}.')

    # Select 90 % for training and 10 % for validation
val_pid_selection = get_selection(len(data_pid), 0.1)
data_pid_val = data_pid[val_pid_selection == 1]
data_pid = data_pid[val_pid_selection == 0]

print('--- Saving save PID data to storage ---')
t = time()
    # Saving to storage
with h5py.File(args.pwd_storage + args.filename + "_pid_sel_test.h5", 'w') as hf:
    for var in var_list + ['pid_labels', 'weight']:
        hf.create_dataset('train/' + var, data=data_pid[var], compression = 'lzf')
        hf.create_dataset('val/' + var, data=data_pid_val[var], compression = 'lzf')
print(f'Finished Saving to storage - Time spent: {timediftostr(t)}.')
            
    # ISO Signal/bkg selection
    # Signal:     abs(ll_m - mZ) < 10 GeV & LHLoose
    # Background: abs(ll_m - mZ) > 15 GeV & !LHLoose

print('--- Selecting ISO Data and Labels ---')
ISO_sig = (np.abs(data['ll_m'] - 91.2) < 10) & (data['p_LHLoose'] == 1)
ISO_bkg = (np.abs(data['ll_m'] - 91.2) > 15) & (data['p_LHLoose'] == 0)
ISO_event_sel = ((ISO_sig) | (ISO_bkg))
data_iso = data[ISO_event_sel]
label_iso = ((np.abs(data_iso['ll_m'] - 91.2) < 10) & (data_iso['p_LHLoose'] == 1))*1

print('--- Reweighting and appending labels ---')
t = time()

    # Reweighting
data_iso = hep_reweight_array(data_iso, label_iso, is_MC=True)
    # Appending Label field
data_iso = np.lib.recfunctions.append_fields(data_iso, 'iso_labels', label_iso, usemask = False)
print(f'Finished Reweighting and Appending - Time spent: {timediftostr(t)}.')

    # Select 90 % for training and 10 % for validation
val_iso_selection = get_selection(len(data_iso), 0.1)
data_iso_val = data_iso[val_iso_selection == 1]
data_iso = data_iso[val_iso_selection == 0]

print('--- Saving save ISO data to storage ---')
t = time()
    # Saving to storage
with h5py.File(args.pwd_storage + args.filename + "_iso_sel_test.h5", 'w') as hf:
    for var in var_list + ['iso_labels', 'weight']:
        hf.create_dataset('train/' + var, data=data_iso[var], compression = 'lzf')
        hf.create_dataset('val/' + var, data=data_iso[var], compression = 'lzf')
print(f'Finished Saving to storage - Time spent: {timediftostr(t)}.')
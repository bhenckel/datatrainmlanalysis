import numpy as np
import numpy.lib.recfunctions
from hep_ml.reweight import GBReweighter
from datetime import timedelta
from time import time
import h5py
import os

np.random.seed(42)

def load_data(filepath = '', var_list = '', setnames = ['']):
    data_dict = {}
    with h5py.File(filepath, 'r') as hf:
        
        if not isinstance(var_list, list):
            var_list = list(hf[setnames[0][:-1]].keys())
        
        for setname in setnames:
            
            dtype = [(var, str(hf[setname + f'{var}'].dtype)) for var in var_list]
            data = np.recarray((hf[setname + f'{var_list[0]}'].shape[0],), dtype = dtype)
                
            for var in var_list:
                data[var] = hf[setname + f'{var}'][:len(data)]
            
            data_dict[setname[:-1]] = data
    
    if(len(setnames)==1):
        return var_list, data_dict[setnames[0][:-1]]
    else:
        return var_list, data_dict

def timediftostr(t):
    sec = timedelta(seconds=time() - t)
    sec = sec - timedelta(microseconds=sec.microseconds)
    return str(sec)
    

def hep_reweight_array(data, labels, is_MC=True):
    """ This function takes in a numpy array and reweighs E_t, eta and <mu>,
        by gradient boosting."""

    if is_MC:
        
            # Create weight estimators and fit them to DataFrame
        reweighter  = GBReweighter(n_estimators = 200, max_depth = 5)

        reweighter.fit(original = np.array([a.tolist() for a in data[labels < 0.5][['p_et_calo', 'p_eta', 'averageInteractionsPerCrossing']]]),
                        target = np.array([a.tolist() for a in data[labels >= 0.5][['p_et_calo', 'p_eta', 'averageInteractionsPerCrossing']]]))
        print('Fitting of weights is done')

        ratio = np.sum(labels >= 0.5) / np.sum(labels < 0.5)

        total_weight = reweighter.predict_weights(np.array([a.tolist() for a in data[labels < 0.5][['p_et_calo', 'p_eta', 'averageInteractionsPerCrossing']]]))
        print('Predicting of weights is done')
        total_weight = ratio * total_weight / np.mean(total_weight)
        weight = np.ones(len(labels))
        weight[labels < 0.5] = total_weight
        total_label = 'weight'
        
        try:
            data[total_label] = weight
        except ValueError:
            data = numpy.lib.recfunctions.append_fields(data, total_label, weight, usemask = False)
        
        del reweighter
        
    return data

def Build_Folder_Structure(directory_list):
    """ Create folders and parent folders recursively if they doesn't exist already."""
    for dirs in directory_list:
        try:
            os.makedirs(dirs)
        except OSError:
            continue

def get_training_vars(wvars = 'None'):
    var_dict = {
                'LH_pdf_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1'],
                
                'LH_conv_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1', 'p_photonConversionType', 
                                'p_photonConversionRadius', 'p_photonVertexConvEtOverPt', 'p_photonVertexConvPtRatio'],
                            
                'LH_bin_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1', 'p_photonConversionType', 
                                'p_photonConversionRadius', 'p_photonVertexConvEtOverPt', 'p_photonVertexConvPtRatio', 
                                'p_eta', 'p_et_calo', 'averageInteractionsPerCrossing'],
                                 
                'LH_ext_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1', 'p_photonConversionType', 
                                'p_photonConversionRadius', 'p_photonVertexConvEtOverPt', 'p_photonVertexConvPtRatio', 
                                'p_eta', 'p_et_calo', 'averageInteractionsPerCrossing', 'p_maxEcell_time', 
                                'p_maxEcell_energy', 'p_core57cellsEnergyCorrection', 'p_r33over37allcalo'],
                
                'LH_noconv_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                   'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1', 'p_photonConversionType', 
                                   'p_eta', 'p_et_calo', 'averageInteractionsPerCrossing', 'p_maxEcell_time', 
                                   'p_maxEcell_energy', 'p_core57cellsEnergyCorrection', 'p_r33over37allcalo'],
                
                'LH_nocell_vars': ['p_Rhad1', 'p_Rhad', 'p_Reta', 'p_weta2', 'p_Rphi', 'p_weta1', 'p_wtots1', 
                                   'p_fracs1', 'p_DeltaE', 'p_Eratio', 'p_f1', 'p_photonConversionType', 
                                   'p_photonConversionRadius', 'p_photonVertexConvEtOverPt', 'p_photonVertexConvPtRatio', 
                                   'p_eta', 'p_et_calo', 'averageInteractionsPerCrossing', 'p_core57cellsEnergyCorrection', 'p_r33over37allcalo'],
                
                'electron_LH_pdf_vars': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                         'p_Eratio', 'p_f1', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                         'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_TRTPID'],
                
                'electron_LH_bin_vars': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                         'p_Eratio', 'p_f1', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                         'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_TRTPID', 'p_eta', 'p_et_calo', 'NvtxReco'],
                
                'electron_LH_cut_vars': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                         'p_Eratio', 'p_f1','p_numberOfInnermostPixelHits', 'p_numberOfPixelHits',
                                         'p_numberOfSCTHits', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                         'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_EptRatio', 'p_TRTPID', 
                                         'p_wtots1', 'p_eta', 'p_et_calo', 'NvtxReco'],
                
                'electron_vars_ext': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                      'p_Eratio', 'p_f1','p_numberOfInnermostPixelHits', 'p_numberOfPixelHits',
                                      'p_numberOfSCTHits', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                      'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_EptRatio', 'p_TRTPID', 
                                      'p_wtots1', 'p_eta', 'p_et_calo', 'NvtxReco', 'p_E3x5_Lr0', 'p_E7x11_Lr2', 'p_E3x5_Lr1',
                                      'p_ambiguityType', 'p_deltaEta2', 'p_pt_track', 
                                      'p_deltaEta0', 'p_f1core', 'p_fracs1',
                                      'averageInteractionsPerCrossing', 'p_E7x11_Lr3', 'p_core57cellsEnergyCorrection','p_nTracks'],
                
                'electron_vars_final': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                        'p_Eratio', 'p_f1','p_numberOfInnermostPixelHits', 'p_numberOfPixelHits',
                                        'p_numberOfSCTHits', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                        'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_EptRatio', 'p_TRTPID', 
                                        'p_wtots1', 'p_eta', 'p_et_calo', 'NvtxReco', 'p_E7x11_Lr3', 'p_core57cellsEnergyCorrection','p_nTracks'],
                                        
                'electron_vars_ext2': ['p_Rhad1', 'p_Rhad', 'p_f3', 'p_weta2', 'p_Rphi', 'p_Reta',
                                      'p_Eratio', 'p_f1','p_numberOfInnermostPixelHits', 'p_numberOfPixelHits',
                                      'p_numberOfSCTHits', 'p_d0', 'p_d0Sig', 'p_dPOverP',
                                      'p_deltaEta1', 'p_deltaPhiRescaled2', 'p_EptRatio', 'p_TRTPID', 
                                      'p_wtots1', 'p_eta', 'p_et_calo', 'NvtxReco', 'p_E3x5_Lr0', 'p_E7x11_Lr2', 'p_E3x5_Lr1',
                                      'p_ambiguityType', 'p_deltaEta2', 'p_pt_track', 
                                      'p_deltaEta0', 'p_f1core', 'p_fracs1',
                                      'averageInteractionsPerCrossing', 'p_E7x11_Lr3', 'p_core57cellsEnergyCorrection','p_nTracks', 'p_etaModCalo', 'p_phiModCalo'],
                                  
                'iso': ['p_topoetcone20', 'p_topoetcone30', 'p_topoetcone40', 'p_ptvarcone20', 'p_ptvarcone30', 'p_ptvarcone40', 'p_et_calo', 'NvtxReco', 'averageInteractionsPerCrossing']
                }
    variables = var_dict[wvars]
    variables.sort(key=str.lower)
    return variables

def get_selection(data_len, frac):
    n_frac = int(data_len*frac)
    selection = np.array([1] * n_frac + [0] * (data_len-n_frac))
    np.random.shuffle(selection)
    return selection

def get_corrcoef(pid_score, iso_score):
    return np.corrcoef(pid_score, iso_score)[1][0]
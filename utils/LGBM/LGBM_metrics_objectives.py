#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 11 10:44:07 2018

@author: lukas
"""
import joblib
import numpy as np
from scipy.stats import norm
from sklearn.metrics import roc_curve, roc_auc_score, mean_absolute_error

auc_low = 0
auc_high = 1

seed = 42

if joblib.cpu_count() >= 30:
    cpu_n_jobs = 15
else:
    cpu_n_jobs = joblib.cpu_count()-1

# AUC between 90% and 99% signal efficiency value
def auc_a_b(y_true, y_pred, weight, a, b):

    fpr, tpr, _ = roc_curve(y_true, y_pred, sample_weight=weight)
    mask = tpr < a

    tpr_a = tpr[np.sum(mask)-1:]
    fpr_a = fpr[np.sum(mask)-1:]
    tpr_a[0] = a

    mask = tpr_a < b
    tpr_a_b = tpr_a[:np.sum(mask)]
    fpr_a_b = fpr_a[:np.sum(mask)]

    tpr_a_b[-1] = b
    diff = np.diff(tpr_a_b)
    auc_a_b = 0
    for i, x in enumerate(diff):
        auc_a_b += x * fpr_a_b[i]
    
    return auc_a_b

# Evaluation
def auc_a_b_eval(preds, train_data):
    global auc_low, auc_high
    auc_a_b_value = -auc_a_b(train_data.get_label(), preds, train_data.get_weight(), auc_low, auc_high)
    return f'auc_{auc_low}_{auc_high}_cv', auc_a_b_value, False


def auc_eval(preds, train_data):
    auc_value = -roc_auc_score(train_data.get_label(), preds, sample_weight = train_data.get_weight())
    return 'auc_eval', auc_value, False

# Log Cosh
# Objective for Training
def log_cosh_fobj(preds, train_data):
    x = (preds - train_data.get_label())
    grad = np.tanh(x)
    hess = (1 - np.tanh(x)**2)
    return grad, hess

def log_cosh(y_true, y_pred):
    return np.sum(np.log(np.cosh(y_true - y_pred)))

def log_cosh_eval(preds, train_data):
    log_cosh_value = log_cosh(train_data.get_label(), preds)
    return 'log-cosh-eval', log_cosh_value, False

# Mean absolute error
def mae_eval(preds, train_data):
    mae_value = mean_absolute_error(train_data.get_label(), preds)
    return 'mae-eval', mae_value, False

# Measure of performance of the model
def ICE(data, percent=0.25):
    n = len(data)
    upper = int(n * (1-percent))
    lower = int(n * percent)
    sorted_data = np.sort(data)
    IQR = sorted_data[upper] - sorted_data[lower]
    phi = norm.isf(percent)
    value = IQR / (2 * phi)

    return value

# Significance
def significance(preds, data):
    
    sig_final = -999
    
    for x in np.linspace(min(preds), max(preds), num=21):
        mask = preds > x
        
        Signal = sum(data.get_weight()[mask & (data.get_label() == 1)])
        Background = sum(data.get_weight()[(~mask) & (data.get_label() == 0)])
        
        if( ( Background > 0 ) & ( Signal > 0) ): 
            sig_temp = np.sqrt( 2 * ( (Signal + Background) * np.log(1 + (Signal/Background)) - Signal))

            if(sig_temp > sig_final):
                sig_final = sig_temp
            
    return sig_final

def significance_eval(preds, data):
    
    signi = significance(preds, data)
    
    return 'significance-eval', signi, True

def asimov(sig, bkg):
    return np.sqrt(2*((sig+bkg)*np.log(1+(sig/bkg))-sig))

def significancemetric(y_pred, data):
    weights = data.get_weight()
    y_true = data.get_label()
    # Above 0.95 may become unstable (give inf)
    clist = np.linspace(.6, 0.95, 50)
    slist = [weights[(y_true==1)&(y_pred>c)].sum() for c in clist]
    blist = [weights[(y_true==0)&(y_pred>c)].sum() for c in clist]
    z = [asimov(s,b) if s>0 and b>0 else -1 for (s,b) in zip(slist,blist)]
    max_signi = np.max(z)
    return 'signi-metric', max_signi, True

def find_feval(feval):
    if(feval == 'None'): return None
    elif(feval == 'auc'): return auc_eval
    elif(feval == 'mae'): return mae_eval
    elif(feval == 'auc_a_b'): return auc_a_b_eval
    elif(feval == 'signi'): return significancemetric

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 16:18:50 2018

@author: stefan
"""
import joblib
import numpy as np
import matplotlib.pyplot as plt
import lightgbm as lgb
from skopt.utils import use_named_args
from skopt import gp_minimize, forest_minimize, callbacks
import matplotlib.pyplot as plt

from utils.LGBM.LGBM_my_callbacks import *
from utils.LGBM.LGBM_metrics_objectives import *


seed = 42
if joblib.cpu_count() >= 30:
    cpu_n_jobs = 15
else:
    cpu_n_jobs = joblib.cpu_count()-1

def hyper_parameter_tuning(space, train_data, train_label,
                           valid_data, valid_label,
                           training_vars,
                           obj,
                           eval1,
                           train_weight=None,
                           valid_weight=None,
                           early_stopping_rounds=1000,
                           num_boost_round = 50000,
                           ncalls = 25,
                           n_rand_starts = 5,
                           log = None):

    train = lgb.Dataset(np.array([train_data[var] for var in training_vars]).T,
                    label = train_label,
                    weight= train_weight,
                    feature_name=training_vars)
    valid = train.create_valid(np.array([valid_data[var] for var in training_vars]).T,
                   label= valid_label, weight = valid_weight)
    
    @use_named_args(space)
    def objective(**params):
        results = {}

        params['n_jobs'] = cpu_n_jobs
        
        if callable(obj):
            fobj = obj
        else:
            params['objective'] = obj
            fobj = None
        
        params['metric'] = 'None'
        params['boosting_type'] = 'gbdt'
        params['learning_rate'] = 0.05
        
        if not log == None: log.info(f'... Running Iteration ({objective.counter}/{ncalls}) ...')
        
        lgb.train(params = params, train_set = train, num_boost_round = num_boost_round,
                  valid_sets=[valid], valid_names=['valid'], evals_result = results,
                  early_stopping_rounds = early_stopping_rounds, verbose_eval = int(early_stopping_rounds/2),
                  feval = eval1, fobj = fobj)
        
        best_result = min(results['valid'][[*results['valid']][0]])
        ntrees = np.argmin(results['valid'][[*results['valid']][0]])
        if not log == None: log.info(f'Best Iteration: {best_result}')
        if not log == None: log.info(f'Number of trees: {ntrees+1}')
        objective.counter += 1
        return best_result
    
    objective.counter = 1
    results = gp_minimize(objective, space, n_calls=ncalls, n_random_starts = n_rand_starts, random_state=0, verbose = True) #callback=[checkpoint_saver],
    
    print(f'Best score: {results.fun}')
    print(f'Best Parameters: {results.x}')
    
    return results

def hyper_parameter_tuning_rf(space, train_data, train_label,
                              valid_data, valid_label,
                              training_vars,
                              obj,
                              eval1,
                              train_weight=None,
                              valid_weight=None,
                              early_stopping_rounds=1000,
                              num_boost_round = 50000,
                              ncalls = 25,
                              n_rand_starts = 5,
                              log = None):

    train = lgb.Dataset(np.array([train_data[var] for var in training_vars]).T,
                    label = train_label,
                    weight= train_weight,
                    feature_name=training_vars)
    valid = train.create_valid(np.array([valid_data[var] for var in training_vars]).T,
                   label= valid_label, weight = valid_weight)
    
    @use_named_args(space)
    def objective(**params):
        results = {}

        params['n_jobs'] = cpu_n_jobs
        
        if callable(obj):
            fobj = obj
        else:
            params['objective'] = obj
            fobj = None
        
        params['metric'] = 'None'
        params['boosting_type'] = 'gbdt'
        params['learning_rate'] = 0.05
        
        if not log == None: log.info(f'... Running Iteration ({objective.counter}/{ncalls}) ...')
        
        lgb.train(params = params, train_set = train, num_boost_round = num_boost_round,
                  valid_sets=[valid], valid_names=['valid'], evals_result = results,
                  early_stopping_rounds = early_stopping_rounds, verbose_eval = int(early_stopping_rounds/2),
                  feval = eval1, fobj = fobj)
        
        best_result = min(results['valid'][[*results['valid']][0]])
        ntrees = np.argmin(results['valid'][[*results['valid']][0]])
        if not log == None: log.info(f'Best Iteration: {best_result}')
        if not log == None: log.info(f'Number of trees: {ntrees+1}')
        objective.counter += 1
        return best_result
    
    objective.counter = 1
    results = forest_minimize(objective, space, n_calls=ncalls, n_random_starts = n_rand_starts, random_state=0, verbose = True) #callback=[checkpoint_saver],
    
    print(f'Best score: {results.fun}')
    print(f'Best Parameters: {results.x}')
    
    return results

def train_model(params, train_data, train_label,
                valid_data, valid_label,
                train_vars,
                objective,
                eval,
                early_stopping_rounds = 1000,
                train_weight = None,
                valid_weight = None,
                num_boost_round = 1000,
                learning_rate = None):
    """ Train model to data using params and labels. Each instance
        is weighted by weights array. Returns a trained model and the parameters
        used for training (for debugging and sanity-checking purposes)."""

    train = lgb.Dataset(np.array([train_data[var] for var in train_vars]).T,
                    label = train_label,
                    weight= train_weight,
                    feature_name=train_vars)
    valid = train.create_valid(np.array([valid_data[var] for var in train_vars]).T,
                   label= valid_label, weight = valid_weight)
    
    if not callable(objective):
        params['objective'] = objective
        fobj = None
    else:
        fobj = objective
    
    params['metric'] = 'None'
    
    result = {}
    model = lgb.train(params = params, train_set = train, num_boost_round = num_boost_round,
                      valid_sets=[valid], valid_names=['valid', 'train'], feval = eval, fobj = fobj,
                      learning_rates = learning_rate, verbose_eval = 100, evals_result = result,
                      early_stopping_rounds = early_stopping_rounds)

    return model, result


def predict(data, model, training_vars):
    """ Uses a trained model to predict on data to produce 'prediction'.
        From these, a BDT score is extracted and retured."""

    prediction = model.predict(np.array([data[var] for var in training_vars]).T)
    return prediction

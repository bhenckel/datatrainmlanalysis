#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 14:01:40 2018

@author: lukas
"""

import joblib
import lightgbm as lgb
import numpy as np

if joblib.cpu_count() >= 30:
    cpu_n_jobs = 15
else:
    cpu_n_jobs = joblib.cpu_count()-1


def lgbm_early_stop(stopping_rounds, verbose=False):
    """Create a callback that activates early stopping.

    Note
    ----
    Activates early stopping.
    The model will train until the validation score stops improving.
    Validation score needs to improve at least every ``early_stopping_rounds`` round(s)
    to continue training.
    Requires at least one validation data and one metric.
    If there's more than one, will check all of them. But the training data is ignored anyway.

    Parameters
    ----------
    stopping_rounds : int
       The possible number of rounds without the trend occurrence.

    verbose : bool, optional (default=True)
        Whether to print message with early stopping information.

    Returns
    -------
    callback : function
        The callback that activates early stopping.
    """
    best_score = []
    best_iter = []
    best_score_list = []
    cmp_op = []

    def init(env):
        """internal function"""
        if not env.evaluation_result_list:
            raise ValueError('For early stopping, '
                             'at least one dataset and eval metric is required for evaluation')

        if verbose:
            msg = "Training until validation scores don't improve for {} rounds."
            print(msg.format(stopping_rounds))

        for eval_ret in env.evaluation_result_list:
            best_iter.append(0)
            best_score_list.append(None)
            if eval_ret[3]:
                best_score.append(float('-inf'))
                cmp_op.append(lgb.callback.gt)
            else:
                best_score.append(float('inf'))
                cmp_op.append(lgb.callback.lt)

    def callback(env):
        """internal function"""
        if not cmp_op:
            init(env)
        for i in lgb.callback.range_(len(env.evaluation_result_list)):
            score = env.evaluation_result_list[i][2]
            if cmp_op[i](score, best_score[i]):
                best_score[i] = score
                best_iter[i] = env.iteration
                best_score_list[i] = env.evaluation_result_list
            elif env.iteration - best_iter[i] >= stopping_rounds:
                if verbose:
                    print('Early stopping, best iteration is:\n[%d]\t%s' % (
                        best_iter[i] + 1, '\t'.join([lgb.callback._format_eval_result(x) for x in best_score_list[i]])))
                raise lgb.callback.EarlyStopException(env.iteration, score)
            if env.iteration == env.end_iteration - 1:
                if verbose:
                    print('Did not meet early stopping. Best iteration is:\n[%d]\t%s' % (
                        best_iter[i] + 1, '\t'.join([lgb.callback._format_eval_result(x) for x in best_score_list[i]])))
                raise lgb.callback.EarlyStopException(env.iteration, score)
    callback.order = 30
    return callback

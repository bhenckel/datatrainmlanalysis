# Variables that will not be saved in the hdf5 files if they are in the ROOT TTree
def remove_branches():
    return ['p_cell_deta',
            'p_cell_dphi',
            'p_cell_electronicnoise',
            'p_cell_energy',
            'p_cell_eta',
            'p_cell_phi',
            'p_cell_sampling',
            'p_cell_time',
            'p_cell_totalnoise',
            'ph.noFF_deltae', 
            'ph.noFF_e277',
            'ph.noFF_rhad1', 
            'ph.noFF_rhad',
            'ph.noFF_reta', 
            'ph.noFF_rphi',
            'ph.noFF_weta2', 
            'ph.noFF_f1',
            'ph.noFF_fside', 
            'ph.noFF_wstot',
            'ph.noFF_w1', 
            'ph.noFF_eratio',
            'ph.noFF_tight_id']


# Variables that will be scaled by the RobustScaler/QuantileTransformer
def scale_vars():
    # all scalar variables
    variables = [
             'p_eta',
             'p_d0',
             'p_deltaPhiRescaled2',
             'p_pt_track',
             'p_qOverP',
             'p_deltaPhiFromLastMeasurement',
             'p_deltaPhiRescaled0',
             'p_deltaEta2',
             'p_deltaEta3',
             'p_sigmad0',
             'p_charge',
             'p_nTracks',
             'BC_distanceFromFront',
             'BC_filledBunches',
             'correctedScaledAverageMu',
             'correctedScaledActualMu',
             'NvtxReco',
             'p_ambiguityType',
             'p_deltaEta0',
             'p_TRTPID',
             'p_d0Sig',
             'p_dPOverP',
             'p_deltaEta1',
             'p_numberOfInnermostPixelHits',
             'p_eAccCluster',
             'p_cellIndexCluster',
             'p_etaModCalo', 
             'averageInteractionsPerCrossing',  
             'p_poscs2', 
             'p_dPhiTH3',
             'p_eClusterLr0',
             'p_eClusterLr1',
             'p_eClusterLr2',
             'p_eClusterLr3',
             'p_photonConversionType',
          	 'p_photonConversionRadius',
         	 'p_photonVertexPtConvDecor',
         	 'p_photonVertexPtConv',
         	 'p_photonVertexPt1',
          	 'p_photonVertexPt2',
         	 'p_photonVertexConvPtRatio',
         	 'p_photonVertexConvEtOverPt',
         	 'p_photonVertexRconv',
         	 'p_photonVertexzconv',
         	 'p_photonVertexPixHits1',
         	 'p_photonVertexSCTHits1',
         	 'p_photonVertexPixHits2',
         	 'p_photonVertexSCTHits2',  
             'p_et_calo',
             'p_numberOfPixelHits', 
             'p_numberOfSCTHits', 
             'p_EptRatio', 
             'p_Rhad1', 
             'p_Rhad', 
             'p_f3', 
             'p_weta2', 
             'p_Rphi', 
             'p_Reta',
             'p_Eratio', 
             'p_f1',
             'p_wtots1'
    ]
    
    # track variables, since they need special treatment to scale them properly
    tracks = [
             'tracks_pt',
             'tracks_eta',
             'tracks_phi',
             'tracks_charge',
             'tracks_chi2',
             'tracks_ndof',
             'tracks_pixhits',
             'tracks_scthits',
             'tracks_trthits',
             'tracks_vertex',
             'tracks_z0',
             'tracks_d0',
             'tracks_dR',
             'tracks_theta',
             'tracks_sigmad0',
             'p_tracks_pt',
             'p_tracks_eta',
             'p_tracks_phi',
             'p_tracks_charge',
             'p_tracks_chi2',
             'p_tracks_ndof',
             'p_tracks_pixhits',
             'p_tracks_scthits',
             'p_tracks_trthits',
             'p_tracks_vertex',
             'p_tracks_z0',
             'p_tracks_d0',
             'p_tracks_dR',
             'p_tracks_theta',
             'p_tracks_sigmad0'
             ]
             
    # Variables that are only available for electrons
    electron = [
            'p_deltaPhiRescaled2',
            'p_deltaEta0',
            'p_deltaEta1',
            'p_deltaEta2',
            'p_deltaEta3',
            'p_TRTPID',
            'p_deltaPhiFromLastMeasurement',
            'p_numberOfSCTHits',
            'p_numberOfInnermostPixelHits',
            'p_numberOfPixelHits',
            'p_numberOfTRTHits',
            'p_pt_track',
            'p_dPOverP',
            'p_qOverP',
            'p_EptRatio',
            'p_deltaPhiRescaled0',
            'p_d0Sig',
            'p_d0',
            'p_sigmad0',
            'p_charge',
            'p_nTracks'
            
    ]

    # Variables that are only available for photons
    photon = [
             'p_photonConversionType',
         	 'p_photonConversionRadius',
        	 'p_photonVertexPtConvDecor',
        	 'p_photonVertexPtConv',
        	 'p_photonVertexPt1',
         	 'p_photonVertexPt2',
        	 'p_photonVertexConvPtRatio',
        	 'p_photonVertexConvEtOverPt',
        	 'p_photonVertexRconv',
        	 'p_photonVertexzconv',
        	 'p_photonVertexPixHits1',
        	 'p_photonVertexSCTHits1',
        	 'p_photonVertexPixHits2',
        	 'p_photonVertexSCTHits2'    
    ]

    return variables, tracks, electron, photon

# Variables for which outliers will be set to the median of the distribution
def outlier_vars():
    outlier_vars = [
        'p_deltaPhiRescaled2',
        'p_deltaEta1',
        'p_deltaEta0',
        'p_deltaEta2',
        'p_deltaEta3'
    ]

    return outlier_vars
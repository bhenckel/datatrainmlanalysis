#include <TChain.h>
#include <TFile.h>

void collect_data17_photon() {
    TChain* chain = new TChain("ga_tree");
    
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_EGAM4.grp17_v01_p3948_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodB.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodC.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodD.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodE.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodF.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodH.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodI.physics_Main.PhysCont.DAOD_EGAM3.repro21_v01_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/bhenckel/storage/grid/ntuple/user.bhenckel.data17_13TeV.periodK.physics_Main.PhysCont.DAOD_EGAM3.repro21_v02_v01_myOutput.root/*.root");
    
    TFile* file = TFile::Open("/groups/hep/bhenckel/work/DataTrainMLAnalysis/storage/data17_photon.root", "RECREATE");
    
    chain->SetBranchStatus("*", 0);
        // Event info
    chain->SetBranchStatus("eventNumber", 1);
    chain->SetBranchStatus("mcChannelNumber", 1);
    chain->SetBranchStatus("runNumber", 1);
    chain->SetBranchStatus("actualInteractionsPerCrossing", 1);
    chain->SetBranchStatus("averageInteractionsPerCrossing", 1);
    chain->SetBranchStatus("isMC", 1);
    chain->SetBranchStatus("analysisType", 1);
    chain->SetBranchStatus("NvtxReco", 1);
    chain->SetBranchStatus("met_met", 1);
    chain->SetBranchStatus("met_phi", 1);
    chain->SetBranchStatus("ll_m", 1);
    chain->SetBranchStatus("ll_pt", 1);
    chain->SetBranchStatus("ll_eta", 1);
    chain->SetBranchStatus("ll_phi", 1);
    chain->SetBranchStatus("llg_m", 1);
    chain->SetBranchStatus("llg_pt", 1);
    chain->SetBranchStatus("llg_eta", 1);
    chain->SetBranchStatus("llg_phi", 1);
    chain->SetBranchStatus("p_et_calo", 1);
    chain->SetBranchStatus("p_eta", 1);
    chain->SetBranchStatus("p_phi", 1);
    chain->SetBranchStatus("tag1_charge", 1);
    chain->SetBranchStatus("tag2_charge", 1);
    chain->SetBranchStatus("p_mTransW", 1);
    chain->SetBranchStatus("tag1_type", 1);
    chain->SetBranchStatus("tag2_type", 1);
    chain->SetBranchStatus("p_photonIsLooseEM", 1);
    chain->SetBranchStatus("p_photonIsTightEM", 1);
    chain->SetBranchStatus("photon_pdfvars_score", 1);
    chain->SetBranchStatus("photon_convvars_score", 1);
    chain->SetBranchStatus("photon_binvars_score", 1);
    chain->SetBranchStatus("photon_iso_score", 1);
    chain->SetBranchStatus("photon_pdfvars_score_logit", 1);
    chain->SetBranchStatus("photon_convvars_score_logit", 1);
    chain->SetBranchStatus("photon_binvars_score_logit", 1);
    chain->SetBranchStatus("photon_iso_score_logit", 1);
        // Photon PID vars
    chain->SetBranchStatus("p_Rhad1", 1);
    chain->SetBranchStatus("p_Rhad", 1);
    chain->SetBranchStatus("p_weta2", 1);
    chain->SetBranchStatus("p_Rphi", 1);
    chain->SetBranchStatus("p_Reta", 1);
    chain->SetBranchStatus("p_Eratio", 1);
    chain->SetBranchStatus("p_f1", 1);
    chain->SetBranchStatus("p_wtots1", 1);
    chain->SetBranchStatus("p_weta1", 1);
    chain->SetBranchStatus("p_fracs1", 1);
    chain->SetBranchStatus("p_DeltaE", 1);
        // Photon PID ext vars
    chain->SetBranchStatus("p_core57cellsEnergyCorrection", 1);
    chain->SetBranchStatus("p_r33over37allcalo", 1);
    chain->SetBranchStatus("p_etaModCalo", 1);
    chain->SetBranchStatus("p_phiModCalo", 1);
    chain->SetBranchStatus("p_photonConversionType", 1);
    chain->SetBranchStatus("p_photonConversionRadius", 1);
    chain->SetBranchStatus("p_photonVertexConvEtOverPt", 1);
    chain->SetBranchStatus("p_photonVertexConvPtRatio", 1);
        // Photon ISO vars
    chain->SetBranchStatus("p_topoetcone20", 1);
    chain->SetBranchStatus("p_topoetcone30", 1);
    chain->SetBranchStatus("p_topoetcone40", 1);
    chain->SetBranchStatus("p_ptvarcone20", 1);
    chain->SetBranchStatus("p_ptvarcone30", 1);
    chain->SetBranchStatus("p_ptvarcone40", 1);
    
    chain->CloneTree(-1, "fast");
    
    file->Write();
    delete file;
}
#include <TChain.h>
#include <TFile.h>

void collect_data17() {
    TChain* chain = new TChain("el_tree");
    
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327862.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327582.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00330160.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327761.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327490.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00326446.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00328374.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327265.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327636.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00326657.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00328099.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00330101.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root");
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00326551.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00330079.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00326945.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327057.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327745.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00325713.physics_Main.r10260_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00329829.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00329778.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00330025.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00329964.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327662.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00328393.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00328221.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00326834.physics_Main.r10250_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root"); 
    chain->Add("/groups/hep/ehrke/storage/NtuplefromGrid/Data17/user.lehrke.data17_13TeV.00327764.physics_Main.r10203_p3399.EGAM1.001.ePID19_NTUP0_v01_myOutput.root/*.root");
    
    TFile* file = TFile::Open("/groups/hep/bhenckel/storage/data17.root", "RECREATE");
    
    chain->SetBranchStatus("*", 0);
    // Event info
    chain->SetBranchStatus("eventNumber", 1);
    chain->SetBranchStatus("mcChannelNumber", 1);
    chain->SetBranchStatus("runNumber", 1);
    chain->SetBranchStatus("actualInteractionsPerCrossing", 1);
    chain->SetBranchStatus("averageInteractionsPerCrossing", 1);
    chain->SetBranchStatus("isMC", 1);
    chain->SetBranchStatus("analysisType", 1);
    chain->SetBranchStatus("NvtxReco", 1);
    chain->SetBranchStatus("met_met", 1);
    chain->SetBranchStatus("met_phi", 1);
    chain->SetBranchStatus("ll_m", 1);
    chain->SetBranchStatus("ll_pt", 1);
    chain->SetBranchStatus("ll_eta", 1);
    chain->SetBranchStatus("ll_phi", 1);
    chain->SetBranchStatus("p_et_calo", 1);
    chain->SetBranchStatus("p_pt_track", 1);
    chain->SetBranchStatus("p_eta", 1);
    chain->SetBranchStatus("p_phi", 1);
    chain->SetBranchStatus("p_charge", 1);
    chain->SetBranchStatus("tag_charge", 1);
    chain->SetBranchStatus("p_qOverP", 1);
    chain->SetBranchStatus("p_z0", 1);
    chain->SetBranchStatus("p_d0", 1);
    chain->SetBranchStatus("p_sigmad0", 1);
    chain->SetBranchStatus("p_d0Sig", 1);
    chain->SetBranchStatus("p_LHValue", 1);
    chain->SetBranchStatus("p_mTransW", 1);
    chain->SetBranchStatus("TruthSelection", 1);
    chain->SetBranchStatus("tag_type", 1);
    chain->SetBranchStatus("p_LHLoose", 1);
    chain->SetBranchStatus("p_LHMedium", 1);
    chain->SetBranchStatus("p_LHTight", 1);
    chain->SetBranchStatus("pdf_cut_vars_score", 1);
    chain->SetBranchStatus("pdf_vars_score", 1);
    // Electron PID vars
    chain->SetBranchStatus("p_Rhad1", 1);
    chain->SetBranchStatus("p_Rhad", 1);
    chain->SetBranchStatus("p_f3", 1);
    chain->SetBranchStatus("p_weta2", 1);
    chain->SetBranchStatus("p_Rphi", 1);
    chain->SetBranchStatus("p_Reta", 1);
    chain->SetBranchStatus("p_Eratio", 1);
    chain->SetBranchStatus("p_f1", 1);
    chain->SetBranchStatus("p_numberOfInnermostPixelHits", 1);
    chain->SetBranchStatus("p_numberOfPixelHits", 1);
    chain->SetBranchStatus("p_numberOfSCTHits", 1);
    chain->SetBranchStatus("p_dPOverP", 1);
    chain->SetBranchStatus("p_deltaEta1", 1);
    chain->SetBranchStatus("p_deltaPhiRescaled2", 1);
    chain->SetBranchStatus("p_EptRatio", 1);
    chain->SetBranchStatus("p_TRTPID", 1);
    chain->SetBranchStatus("p_wtots1", 1);
    // Electron PID ext vars
    chain->SetBranchStatus("p_E3x5_Lr0", 1); 
    chain->SetBranchStatus("p_E7x11_Lr2", 1); 
    chain->SetBranchStatus("p_E3x5_Lr1", 1);
    chain->SetBranchStatus("p_ambiguityType", 1); 
    chain->SetBranchStatus("p_deltaEta2", 1); 
    chain->SetBranchStatus("p_deltaEta0", 1); 
    chain->SetBranchStatus("p_f1core", 1); 
    chain->SetBranchStatus("p_fracs1", 1);
    chain->SetBranchStatus("p_E7x11_Lr3", 1); 
    chain->SetBranchStatus("p_core57cellsEnergyCorrection", 1);
    chain->SetBranchStatus("p_nTracks", 1);
    chain->SetBranchStatus("p_etaModCalo", 1);
    chain->SetBranchStatus("p_phiModCalo", 1);
    // Electron ISO vars
    chain->SetBranchStatus("p_topoetcone20", 1);
    chain->SetBranchStatus("p_topoetcone30", 1);
    chain->SetBranchStatus("p_topoetcone40", 1);
    chain->SetBranchStatus("p_ptvarcone20", 1);
    chain->SetBranchStatus("p_ptvarcone30", 1);
    chain->SetBranchStatus("p_ptvarcone40", 1);

    chain->CloneTree(-1, "fast");
    
    file->Write();
    delete file;
}
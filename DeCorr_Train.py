from time import time
import joblib
import numpy as np
import pandas as pd
import lightgbm as lgb

from skopt.space import Real, Integer, Categorical
from skopt import dump, load

import logging as log
import argparse
import zlib
import shap
import gc
import h5py
from numpy.random import shuffle
    # Fixes relative imports
import sys
import os
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    # Current working directory. Used for relative imports of data files
cwd = path.abspath(path.dirname(__file__))+"/"
    # My own scripts/modules
from utils.utils import Build_Folder_Structure, get_training_vars, timediftostr, load_data
from utils.LGBM.LightGBM_functions_v3 import train_model, predict, hyper_parameter_tuning, hyper_parameter_tuning_rf
from utils.LGBM.LGBM_metrics_objectives import find_feval

np.random.seed(seed=42)

parser = argparse.ArgumentParser(description='DeCorr Training for PID/ISO models with the LightGBM framework.')
parser.add_argument('--feval', action='store', type=str, required=True, help='Which objective function? (default = auc)', default='auc')
parser.add_argument('--vars', action='store', type=str, required=True, help='Which vars to use?')
parser.add_argument('--filename', action='store', type=str, required=True, help='Which file to use?')
parser.add_argument('--label', action='store', type=str, required=True, help='Truth label string?')
parser.add_argument('--hypopt', action='store', type=int, required=False, help='Run Hyper optimization (default = True)', default=1)
parser.add_argument('--train', action='store', type=int, required=False, help='Run Train (default = True)', default=1)
parser.add_argument('--predict', action='store', type=int, required=False, help='Run Predict (default = True)', default=1)
parser.add_argument('--shap', action='store', type=int, required=False, help='Run SHAP values (default = False)', default=0)
parser.add_argument('--njobs', action='store', type=int, required=False, help='Amount of njobs (default = 15)', default=15)
parser.add_argument('--note', action='store', type=str, required=False, help='Note for the run', default='')
parser.add_argument('--nice', action='store', type=int, required=False, help='Number to renice', default=10)
parser.add_argument('--train-subsample', action='store', type=float, required=False, help='Fraction of train data used (default = 1.0)', default=1.0)
parser.add_argument('--val-subsample', action='store', type=float, required=False, help='Fraction of val data used (default = 1.0)', default=1.0)
parser.add_argument('--evl-subsample', action='store', type=float, required=False, help='Fraction of evl data used (default = 1.0)', default=1.0)
parser.add_argument('--hp-subsample', action='store', type=float, required=False, help='Fraction of training set to do Hypopt on (default = 0.1)', default=0.2)

args = parser.parse_args()

os.nice(args.nice)
cpu_n_jobs = args.njobs
pwd_storage = cwd + 'storage/'

    # Output and plots directory. Which are both one folder back.
output_directory = cwd+'output/'
    # Importing Training Vars
training_vars = get_training_vars(args.vars)
training_vars.sort(key=str.lower)
varhash = zlib.crc32(bytes(':'.join(training_vars), 'utf-8'))
    # Specific hash for the vars used for this run
output_directory += f'{len(training_vars)}_{varhash}/'
    # Param, model, score and variable directories, realtive to cwd
param_directory    = output_directory+f'params/'
model_directory    = output_directory+f'trained_models/'
score_directory    = output_directory+f'scores/'
Variable_directory = output_directory+f'featimp/'
log_directory      = output_directory+f'logs/'
    # Build folder structure, if it doesn't exist, so save functions won't fail
Build_Folder_Structure([param_directory, model_directory, score_directory, Variable_directory, log_directory])

    # List of parameter settings for Hyper Parameter tuning for LGBM. 
space = [Integer(10, 100, name='num_leaves'),
         Integer(20, 250, name='min_child_samples'),
         Real(10**-5, 10**4, 'log-uniform', name='min_child_weight'), #'min_child_weight' : [0.0001, 0.001, 0.01, 0.1],#[0.0001, 0.001, 0.01, 0.1, 1], #[0.1,0.5,1,2,3,5],
         Real(0.0, 0.4, name='min_split_gain'), #'min_split_gain' : [0.0, 0.1, 0.2, 0.3],#[i/10.0 for i in range(0,6)],
         Real(0.6, 1.0, name='colsample_bytree'),
         Real(0.1, 1.0, name='subsample'),
         Integer(0, 50, name='subsample_freq')]

    # Take feval and return function
feval = find_feval(args.feval)
    # Data Filename
filename = args.filename
    # Setting Up Logging file and level
log_filename = log_directory + f'{args.feval}_' + args.note + '.log'
log.basicConfig(filename=log_filename, filemode='w', format='[%(levelname)s] %(message)s', level=log.INFO)    
    # Defining Filename details for importing and saving.
file_details = f'{args.feval}_' + args.note + '_'    
      # Log INFO for run
log.info(f'--------------- ML INFO --------------')
log.info(f'Evaluation Function: {args.feval}')
log.info(f'Variables:           {args.vars}')
log.info(f'Number of Vars:      {len(training_vars)}')
log.info(f'HP fraction:         {args.hp_subsample}')
log.info(f'Train fraction:      {args.train_subsample}')
log.info(f'Val fraction:        {args.val_subsample}')
log.info(f'Evl fraction:        {args.evl_subsample}')
log.info(f'-------------- RUN INFO --------------')
log.info(f'Datafile:            {args.filename}')
log.info(f'Truth Label:         {args.label}')
log.info(f'Run Hyperopt:        {bool(args.hypopt)}')
log.info(f'Run Training:        {bool(args.train)}')
log.info(f'Run Prediction:      {bool(args.predict)}')
log.info(f'Run SHAP:            {bool(args.shap)}')
log.info(f'-------------- RUN SETTINGS ----------')
log.info(f'Number of threads:   {args.njobs}')
log.info(f'Level of niceness:   {args.nice}')
log.info(f'--------------------------------------')
#%%############################################################################
#   Importing data.
###############################################################################
log.info(f'')
log.info(f'--- Importing Data ---')

t = time()
import_vars = training_vars + [args.label, 'weight']
_, data_dict =  load_data(filepath = pwd_storage + filename, var_list = import_vars, setnames = ['train/', 'val/'])

log.info(f'Finished Importing Data - Time spent: {timediftostr(t)}.')
#%%############################################################################
#   Perform Hyper Parameter tuning and save parameters
###############################################################################
if args.hypopt:
    log.info(f'')
    log.info('--- Running Hyper Parameter Tuning ---')    
    log.info('The following hyper parameters make up the space:')
    log.info([space[i].name for i in range(len(space))])
    
    t_start = time()
    
    train_subsample = data_dict['train'][np.random.choice(data_dict['train'].shape[0], int(len(data_dict['train']) * args.hp_subsample), replace=False)]

    early_stopping_rounds = 50
    train_labels = train_subsample[args.label]
    valid_labels = data_dict['val'][args.label]
    train_weights = train_subsample['weight']
    valid_weights = data_dict['val']['weight']
    obj = 'binary'
    
    results = hyper_parameter_tuning_rf(space, train_subsample, train_labels, 
                                     data_dict['val'], valid_labels, 
                                     training_vars,
                                     obj,
                                     feval,
                                     train_weight=train_weights,
                                     valid_weight=valid_weights,
                                     early_stopping_rounds=early_stopping_rounds,
                                     num_boost_round = 50000,
                                     ncalls = 100,
                                     n_rand_starts = 10,
                                     log = log)
    
    dump(results, param_directory+f'pid_hypopt_results_' + file_details + '.pkl', store_objective=False)
    
    log.info('The Best parameters were found to be the following:')
    log.info([f'{space[i].name}: {results.x[i]}' for i in range(len(space))])
    log.info(f'Finished Hyper Parameter Tuning - Time spent: {timediftostr(t)}.')
elif not args.hypopt:
    log.info(f'')
    log.info('--- Selected NOT to run Hyper Parameter Tuning ---')
    log.info('Importing Results from relevant opmtization')
    
    results = load(param_directory+f'pid_hypopt_results_' + file_details + '.pkl')
     
    log.info('The params and their best value are the following:')
    log.info([f'{space[i].name}: {results.x[i]}' for i in range(len(space))])
#%%############################################################################
#   Load HP tuned model parameters, train on entire training set
###############################################################################
if args.train:
    log.info(f'')
    log.info('--- Running Training ---')
    log.info('Training Variables:')
    log.info(training_vars)
    
    t_start = time()
    
    names = [space[var].name for var in range(len(space))]
    best_params = {names[i]:results.x[i] for i in range(len(names))}
    
    params = {'boosting_type': 'gbdt',
              'class_weight': None,
              'learning_rate': 0.05,
              'max_depth': -1,
              'n_jobs': cpu_n_jobs,
              'objective': None,
              'random_state': None,
              'reg_alpha': 0.0,
              'reg_lambda': 0.0,
              'subsample_for_bin': 200000}
    
    params.update(best_params)
    
    obj = 'binary'
    
    model, results = train_model(params, data_dict['train'], data_dict['train'][args.label],  
                        data_dict['val'], data_dict['val'][args.label], 
                        training_vars,
                        obj,
                        feval,
                        early_stopping_rounds = 500,
                        train_weight = data_dict['train']['weight'],
                        valid_weight = data_dict['val']['weight'],
                        num_boost_round = 50000,
                        learning_rate = None)
    log.info('Saving trained model and training results')
    
    model.save_model(model_directory + f'pid_model_' + file_details + '.txt', num_iteration=-1)
    joblib.dump(results, model_directory + f'pid_train_results_' + file_details)
    
    log.info(f'Finished Training - Time spent: {timediftostr(t)}.')
elif not args.train:
    log.info(f'--- Selected NOT to run Training ---')
    log.info('Creating model with relevant model_file')
    
    model = lgb.Booster(model_file=model_directory + f'pid_model_' + file_details + '.txt')

#%%############################################################################
#   Load pre-trained MC models and predict on test set and save scores,
#   or load pre-predicted scores
###############################################################################
if args.predict:
    log.info(f'')
    log.info('--- Running Prediction Test ---')
    
    t_start = time()
    
    _, eval_dict = load_data(filepath = pwd_storage + f'data17_prep_test.h5', var_list = training_vars, setnames = ['data/', 'evl/'])
    
    pred_data = predict(eval_dict['data'], model, training_vars)
    pred_evl  = predict(eval_dict['evl'], model, training_vars)
    
    joblib.dump(pred_data, score_directory+f'pid_data_preds_' + file_details)
    joblib.dump(pred_evl, score_directory+f'pid_evl_preds_' + file_details)
    
    log.info(f'Finished Predicting - Time spent: {int((time() - t_start)/60):d} minutes.')
    
elif not args.predict:
    log.info(f'--- Selected NOT to predict ---')
    log.info(f'Loading predictions from relevant model')
    
    pred_train = joblib.load(score_directory+f'pid_data_preds_' + file_details)
    pred_evl = joblib.load(score_directory+f'pid_evl_preds_' + file_details)
    
#%%############################################################################
##   Calculate SHAP values
###############################################################################
if args.shap:
    log.info(f'')
    log.info('--- Running Feature Importance ---')
    t = time()
    
    model = lgb.Booster(model_file=model_directory + f'pid_model_' + file_details + '.txt')
    model.reset_parameter({'nthread':cpu_n_jobs})
    
         #SHAP values
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(np.array([data_dict['train'][var] for var in training_vars]).T)               
    
    shap_values_df = pd.DataFrame(shap_values, columns = training_vars)
    
    shap_val = shap_values_df.abs().mean(0)
    
    joblib.dump(shap_val, Variable_directory+f'pid_shap_values_' + file_details)
    
    log.info(f'Finished calculating SHAP values - Time spent: {timediftostr(t)}.')


 